import java.util.ArrayList;

import com.task54s50.Country;
import com.task54s50.Region;

public class App {
    public static void main(String[] args) throws Exception {
      
        ArrayList<Region> listRegionVN = new ArrayList<>();
        Region regionVN1 = new Region("HN", "Ha Noi");
        Region regionVN2 = new Region("DN", "Da Nang");
        Region regionVN3 = new Region("HCM", "Ho Chi Minh");

        listRegionVN.add(regionVN1);
        listRegionVN.add(regionVN2);
        listRegionVN.add(regionVN3);

        ArrayList<Country> listCountry = new ArrayList<>();
        Country country1 = new Country("VN", "Viet Nam", new ArrayList<Region>());
        Country country2 = new Country("USA", "America", new ArrayList<Region>());
        Country country3 = new Country("AUS", "Australia", new ArrayList<Region>());

        listCountry.add(country1);
        listCountry.add(country2);
        listCountry.add(country3);
        country1.setRegions(listRegionVN);

        System.out.println("Danh sach cac country:");
        for (Country country : listCountry) {
            System.out.println(country);
        }
        for (Country country : listCountry) {
            if (country.getCountryCode() == "VN") {
                System.out.println("Danh sach cac region of country VN");
                for (Region regionVN: country.getRegions()){
                    System.out.println(regionVN);
                }
            }
        }
    }
}
